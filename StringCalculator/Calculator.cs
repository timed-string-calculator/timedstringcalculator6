﻿using System;
using System.Collections.Generic;

namespace StringCalculator
{
    public class Calculator
    {
        private readonly string customDelimiterId = "//";
        private readonly string newline = "\n";

        public int Add(string numbers)
        {
            if (string.IsNullOrEmpty(numbers))
            {
                return 0;
            }

            string numbersSection = GetNumbersSection(numbers);
            string[] delimters = GetDelimiters(numbers);
            List<int> numbersList = GetNumbers(delimters, numbersSection);

            ValidateNumbers(numbersList);

            return GetSum(numbersList);
        }

        private void ValidateNumbers(List<int> numbersList)
        {
            List<string> negativeNumbers = new List<string>();

            foreach (var number in numbersList)
            {
                if (number < 0)
                {
                    negativeNumbers.Add(number.ToString());
                }
            }

            if (negativeNumbers.Count != 0)
            {
                throw new Exception($"Negative not allowed {string.Join(",", negativeNumbers)}");
            }
        }

        private string[] GetDelimiters(string numbers)
        {
            var multipleCustomDelimiterId = $"{customDelimiterId}[";
            var multipleCustomDelimiterSeperator = $"]{newline}";
            var multipleCustomDelimiterSplitter = "][";

            if (numbers.StartsWith(multipleCustomDelimiterId))
            {
                var delimiter = numbers.Substring(numbers.IndexOf(multipleCustomDelimiterId) + multipleCustomDelimiterId.Length, numbers.IndexOf(multipleCustomDelimiterSeperator) - (multipleCustomDelimiterSeperator.Length + 1));

                return delimiter.Split(new[] { multipleCustomDelimiterSplitter }, StringSplitOptions.RemoveEmptyEntries);
            }
            else if (numbers.StartsWith(customDelimiterId))
            {
                return new[] { numbers.Substring(numbers.IndexOf(customDelimiterId) + customDelimiterId.Length, numbers.IndexOf(newline) - (newline.Length + 1)) };
            }

            return new[] { ",", newline };
        }

        private string GetNumbersSection(string numbers)
        {
            if (numbers.StartsWith(customDelimiterId))
            {
                return numbers.Substring(numbers.IndexOf(newline) + 1);
            }

            return numbers;
        }

        private int GetSum(List<int> numbersList)
        {
            int sum = 0;

            foreach (var number in numbersList)
            {
                if (number < 1001)
                {
                    sum += number;
                }
            }

            return sum;
        }

        private List<int> GetNumbers(string[] delimters, string numbers)
        {
            string[] numbersArray = numbers.Split(delimters, StringSplitOptions.RemoveEmptyEntries);
            List<int> numbersList = new List<int>();

            foreach (var num in numbersArray)
            {
                if (int.TryParse(num, out int number))
                {
                    numbersList.Add(number);
                }
            }

            return numbersList;
        }
    }
}
